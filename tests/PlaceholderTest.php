<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Tests;

use Infostrates\IbexaContentPageBuilder\InfostratesIbexaContentPageBuilderBundle;
use PHPUnit\Framework\TestCase;

final class PlaceholderTest extends TestCase
{
    public function testPlaceholder(): void
    {
        //TODO please, replace me with real tests T_T
        self::assertSame(
            'Infostrates\IbexaContentPageBuilder\InfostratesIbexaContentPageBuilderBundle',
            InfostratesIbexaContentPageBuilderBundle::class
        );
    }
}
