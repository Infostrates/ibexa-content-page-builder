<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder;

use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Symfony\Bridge\Twig\Extension\HttpKernelRuntime;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PageBuilderTwigExtension extends AbstractExtension
{
    private HttpKernelRuntime $httpKernelRuntime;

    /**
     * @param HttpKernelRuntime $httpKernelRuntime
     */
    public function __construct(HttpKernelRuntime $httpKernelRuntime)
    {
        $this->httpKernelRuntime = $httpKernelRuntime;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'page_builder_blocks',
                [$this, 'renderPageBuilderBlocks'],
                ['needs_context' => true, 'is_safe' => ['html']]
            ),
        ];
    }

    /**
     * @param array<string, mixed> $context
     * @param array<string, mixed> $params
     * @return string
     */
    public function renderPageBuilderBlocks(array $context, array $params = []): string
    {
        $location = $params['location'] ?? $context['location'] ?? null;
        if (!$location instanceof Location) {
            return '';
        }

        return $this->httpKernelRuntime->renderFragment(
            new ControllerReference('ibexa_content:embedAction', [
                'locationId' => $location->id,
                'viewType' => 'pageBuilderBlocks',
                'params' => $params,
            ])
        );
    }
}
