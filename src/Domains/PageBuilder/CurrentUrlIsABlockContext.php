<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use LogicException;

class CurrentUrlIsABlockContext
{
    private ?Location $parentLocation = null;
    private ?Content $content = null;

    public function hasContext(): bool
    {
        return ($this->parentLocation && $this->content);
    }

    public function storeContext(Location $parentLocation, Content $content): void
    {
        $this->parentLocation = $parentLocation;
        $this->content = $content;
    }

    public function getParentLocation(): Location
    {
        if (!$this->parentLocation) {
            throw new LogicException('No context. Check context existence first.');
        }
        return $this->parentLocation;
    }

    public function getContent(): Content
    {
        if (!$this->content) {
            throw new LogicException('No context. Check context existence first.');
        }
        return $this->content;
    }
}
