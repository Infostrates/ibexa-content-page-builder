<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder;

use Infostrates\IbexaContentPageBuilder\Actions\PageBuilderBlocks;
use DomainException;
use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\MVC\Symfony\Event\PreContentViewEvent;
use Ibexa\Core\MVC\Symfony\View\ContentView;
use LogicException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

use function is_array;

class PreviewPageBuilderListener implements EventSubscriberInterface
{
    private RequestStack $requestStack;
    private ContentService $contentService;
    private TagAwareAdapterInterface $cachePool;
    private CurrentUrlIsABlockContext $currentUrlIsABlockContext;

    public function __construct(
        RequestStack $requestStack,
        ContentService $contentService,
        TagAwareAdapterInterface $cachePool,
        CurrentUrlIsABlockContext $currentUrlIsABlockContext
    ) {
        $this->requestStack = $requestStack;
        $this->contentService = $contentService;
        $this->cachePool = $cachePool;
        $this->currentUrlIsABlockContext = $currentUrlIsABlockContext;
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.controller' => 'onKernelController',
            'ezpublish.pre_content_view' => 'onPreContentView',
        ];
    }

    public function onKernelController(ControllerEvent $event): void
    {
        try {
            $semanticPathInfo = $event->getRequest()->get('semanticPathinfo');
            if (!is_string($semanticPathInfo)) {
                throw new DomainException('Unexpected semanticPathInfo type');
            }
            [$previewContentId, $previewVersion] = $this->getPreviewContentIdAndVersion($semanticPathInfo);
        } catch (LogicException $e) {
            return;
        }

        $view = $event->getRequest()->get('view');
        if (!($view instanceof ContentView) || ((int)$view->getContent()->id) !== $previewContentId) {
            return;
        }

        try {
            $versionInfo = $this->contentService->loadVersionInfoById($previewContentId, $previewVersion);
            $content = $this->contentService->loadContentByVersionInfo($versionInfo);

            $view->setContent($content);
        } catch (NotFoundException | UnauthorizedException $e) {
            //Fail silently
        }
    }

    public function onPreContentView(PreContentViewEvent $event): void
    {
        try {
            $masterRequest = $this->requestStack->getMainRequest();
            if (!$masterRequest) {
                throw new LogicException('No master request context');
            }
            $semanticPathInfo = $masterRequest->get('semanticPathinfo');
            if (!is_string($semanticPathInfo)) {
                throw new DomainException('Unexpected semanticPathInfo type');
            }

            [$previewContentId, $previewVersion] = $this->getPreviewContentIdAndVersion($semanticPathInfo);
        } catch (LogicException $e) {
            return;
        }

        $view = $event->getContentView();
        if (
            !($view instanceof ContentView)
            || !$view->hasParameter(PageBuilderBlocks::EMBED_CONTENT_LIST_PARAMETER_NAME)
            || !is_array($view->getParameter(PageBuilderBlocks::EMBED_CONTENT_LIST_PARAMETER_NAME))
        ) {
            return;
        }

        $view->setCacheEnabled(false);
        $this->cachePool->invalidateTags(['content-' . $view->getContent()->id]);

        try {
            $this->currentUrlIsABlockContext->getParentLocation();
        } catch (LogicException $e) {
            return;
        }

        if ($view->getContent()->id !== $this->currentUrlIsABlockContext->getParentLocation()->contentId) {
            return;
        }

        try {
            $versionInfo = $this->contentService->loadVersionInfoById($previewContentId, $previewVersion);
            $previewContent = $this->contentService->loadContentByVersionInfo($versionInfo);
        } catch (NotFoundException | UnauthorizedException $e) {
            throw new DomainException('Unable to get preview content', 0, $e);
        }

        $embedContentList = $view->getParameter(PageBuilderBlocks::EMBED_CONTENT_LIST_PARAMETER_NAME);
        $previewContentMet = false;
        foreach ($embedContentList as $key => $contentItem) {
            if (!($contentItem instanceof Content)) {
                continue;
            }

            if ((int)$contentItem->id !== $previewContentId) {
                continue;
            }

            $previewContentMet = true;
            $embedContentList[$key] = $previewContent;
        }

        if (!$previewContentMet) {
            $embedContentList = array_merge([$previewContent], $embedContentList);
        }
        $view->addParameters([PageBuilderBlocks::EMBED_CONTENT_LIST_PARAMETER_NAME => $embedContentList]);
    }

    /**
     * @param string $semanticPathInfo
     * @return int[]
     */
    private function getPreviewContentIdAndVersion(string $semanticPathInfo): array
    {
        if (!preg_match('#^/content/versionview/(\d+)/(\d+)#', $semanticPathInfo, $matches)) {
            throw new LogicException('Not in a preview version view context');
        }
        [, $previewContentId, $previewVersion] = $matches;

        return [(int)$previewContentId, (int)$previewVersion];
    }
}
