<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\BlockLayoutFieldType;

use Ibexa\Core\FieldType\Value as BaseValue;

class Value extends BaseValue
{
    public ?string $layoutIdentifier;

    public function __construct(?string $layoutIdentifier = null)
    {
        parent::__construct([]);
        $this->layoutIdentifier = $layoutIdentifier;
    }

    public function __toString()
    {
        return (string)$this->layoutIdentifier;
    }
}
