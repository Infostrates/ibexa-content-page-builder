<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\BlockLayoutFieldType;

use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\FieldTypeService;
use Ibexa\AdminUi\Form\DataTransformer\FieldType\FieldValueTransformer;
use Ibexa\Contracts\ContentForms\Data\Content\FieldData;
use Ibexa\Contracts\ContentForms\FieldType\FieldValueFormMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;

class FormMapper implements FieldValueFormMapperInterface
{
    private FieldTypeService $fieldTypeService;

    public function __construct(FieldTypeService $fieldTypeService)
    {
        $this->fieldTypeService = $fieldTypeService;
    }

    /**
     * @param FormInterface $fieldForm
     * @param FieldData     $data
     * @throws NotFoundException
     */
    public function mapFieldValueForm(FormInterface $fieldForm, FieldData $data): void
    {
        $fieldDefinition = $data->fieldDefinition;
        $formConfig = $fieldForm->getConfig();
        $fieldSettings = $fieldDefinition->fieldSettings;

        $fieldForm
            ->add(
                $formConfig->getFormFactory()->createBuilder()
                    ->create(
                        'value',
                        ChoiceType::class,
                        [
                            'choices' => array_flip($fieldDefinition->getFieldSettings()['options']),
                            'preferred_choices' => $fieldSettings['default'] ? [$fieldSettings['default']] : [],
                            'empty_data' => '',
                            'required' => $fieldDefinition->isRequired,
                            'label' => $fieldDefinition->getName(),
                        ]
                    )
                    ->addModelTransformer(
                        new FieldValueTransformer($this->fieldTypeService->getFieldType('isblocklayout'))
                    )
                    ->setAutoInitialize(false)
                    ->getForm()
            )
        ;
    }
}
