<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\BlockLayoutFieldType;

use Ibexa\Core\Persistence\Legacy\Content\FieldValue\Converter\TextLineConverter;
use Ibexa\Core\Persistence\Legacy\Content\StorageFieldDefinition;
use Ibexa\Contracts\Core\Persistence\Content\Type\FieldDefinition;
use JsonException;

class Converter extends TextLineConverter
{
    /**
     * Converts field definition data in $fieldDef into $storageFieldDef.
     *
     * @param FieldDefinition        $fieldDef
     * @param StorageFieldDefinition $storageDef
     * @throws JsonException
     */
    public function toStorageFieldDefinition(FieldDefinition $fieldDef, StorageFieldDefinition $storageDef): void
    {
        $fieldSettings = is_array($fieldDef->fieldTypeConstraints->fieldSettings)
            ? $fieldDef->fieldTypeConstraints->fieldSettings
            : []
        ;

        $default = $fieldSettings['default'] ?? '';
        $options = $fieldSettings['options'] ?? [];

        $storageDef->dataText1 = $default;
        $storageDef->dataText5 = json_encode($options, JSON_THROW_ON_ERROR);
    }

    /**
     * Converts field definition data in $storageDef into $fieldDef.
     *
     * @param StorageFieldDefinition $storageDef
     * @param FieldDefinition        $fieldDef
     */
    public function toFieldDefinition(StorageFieldDefinition $storageDef, FieldDefinition $fieldDef): void
    {
        try {
            $options = json_decode($storageDef->dataText5, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $options = [];
        }

        $default = $storageDef->dataText1;

        $fieldDef->fieldTypeConstraints->fieldSettings = [
            'default' => $default,
            'options' => $options,
        ];
    }
}
