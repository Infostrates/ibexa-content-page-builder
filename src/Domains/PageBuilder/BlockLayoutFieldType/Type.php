<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\BlockLayoutFieldType;

use DomainException;
use Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException;
use Ibexa\Contracts\Core\Repository\Values\ContentType\FieldDefinition;
use Ibexa\Core\Base\Exceptions\InvalidArgumentType;
use Ibexa\Core\FieldType\FieldType;
use Ibexa\Core\FieldType\ValidationError;
use Ibexa\Contracts\Core\FieldType\Value as SPIValue;
use Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\BlockLayoutFieldType\Value;
use RuntimeException;

use function in_array;
use function is_string;

class Type extends FieldType
{
    /** @var array<string, array<string, mixed>> */
    protected $settingsSchema = [
        'default' => [
            'type' => 'string',
            'default' => '',
        ],
        'options' => [
            'type' => 'hash',
            'default' => [],
        ],
    ];

    /**
     * @param array<string, array<string, string>|string> $fieldSettings
     * @return ValidationError[]
     */
    public function validateFieldSettings($fieldSettings): array
    {
        $validationErrors = [];

        foreach ($fieldSettings as $settingKey => $settingValue) {
            if ($settingKey === 'options') {
                if (!is_array($settingValue)) {
                    $validationErrors[] = new ValidationError(
                        "FieldType '%fieldType%' expects setting '%setting%' to be of type '%type%'",
                        null,
                        [
                            '%fieldType%' => $this->getFieldTypeIdentifier(),
                            '%setting%' => $settingKey,
                            '%type%' => 'hash',
                        ],
                        "[$settingKey]"
                    );
                }
            } elseif ($settingKey === 'default') {
                if (!is_array($fieldSettings['options'])) {
                    throw new DomainException('Unexpected $fieldSettings[\'options\'] type');
                }
                $availableValues = array_keys($fieldSettings['options'] ?? []);
                if (!in_array($settingValue, $availableValues, true)) {
                    $validationErrors[] = new ValidationError(
                        "FieldType '%fieldType%' expects setting '%setting%'"
                        . "to be of one of available options '%options%'",
                        null,
                        [
                            '%fieldType%' => $this->getFieldTypeIdentifier(),
                            '%setting%' => $settingKey,
                            '%options%' => implode(',', $availableValues),
                        ],
                        "[$settingKey]"
                    );
                }
            } else {
                $validationErrors[] = new ValidationError(
                    "Setting '%setting%' is unknown",
                    null,
                    [
                        '%setting%' => $settingKey,
                    ],
                    "[$settingKey]"
                );
            }
        }

        return $validationErrors;
    }


    /**
     * @param mixed $validatorConfiguration
     * @return ValidationError[]
     */
    public function validateValidatorConfiguration($validatorConfiguration): array
    {
        if (!empty($validatorConfiguration)) {
            return [new ValidationError('No validator configuration is available for this type')];
        }

        return [];
    }

    /**
     * @param FieldDefinition $fieldDefinition
     * @param SPIValue|Value  $value
     * @return ValidationError[]
     */
    public function validate(FieldDefinition $fieldDefinition, SPIValue $value): array
    {
        $validationErrors = [];

        if ($this->isEmptyValue($value)) {
            return $validationErrors;
        }

        if (!$value instanceof Value) {
            throw new DomainException('Unexpected value type');
        }

        $choices = array_keys($fieldDefinition->getFieldSettings()['options']);
        if (!in_array($value->layoutIdentifier, $choices, true)) {
            $validationErrors = [
                new ValidationError(
                    'Unexpected value : %actualValue% (expect : %expectedValue%)',
                    null,
                    [
                        '%actualValue%' => $value->layoutIdentifier,
                        '%expectedValue%' => implode(', ', $choices),
                    ]
                ),
            ];
        }

        return $validationErrors;
    }

    public function getFieldTypeIdentifier(): string
    {
        return 'isblocklayout';
    }


    public function getName(SPIValue $value, FieldDefinition $fieldDefinition, string $languageCode): string
    {
        throw new RuntimeException(
            'Name generation provided via NameableField set via "ezpublish.fieldType.nameable" service tag'
        );
    }

    /**
     * Returns the fallback default value of field type when no such default
     * value is provided in the field definition in content types.
     *
     * @return Value
     */
    public function getEmptyValue(): Value
    {
        return new Value();
    }

    /**
     * Returns if the given $value is considered empty by the field type.
     *
     * @param SPIValue|Value $value
     *
     * @return bool
     */
    public function isEmptyValue(SPIValue $value): bool
    {
        if (!$value instanceof Value) {
            throw new DomainException('Unexpected value type');
        }

        return $value->layoutIdentifier === null || trim($value->layoutIdentifier) === '';
    }

    /**
     * Inspects given $inputValue and potentially converts it into a dedicated value object.
     *
     * @param string|Value $inputValue
     *
     * @return Value The potentially converted and structurally plausible value.
     */
    protected function createValueFromInput($inputValue)
    {
        if (is_string($inputValue)) {
            $inputValue = new Value($inputValue);
        }

        return $inputValue;
    }

    /**
     * Throws an exception if value structure is not of expected format.
     *
     * @param SPIValue|Value $value
     * @throws InvalidArgumentException If the value does not match the expected structure.
     *
     */
    protected function checkValueStructure(SPIValue $value): void
    {
        if (!$value instanceof Value) {
            throw new DomainException('Unexpected value type');
        }

        if (!is_string($value->layoutIdentifier)) {
            throw new InvalidArgumentType(
                '$value->$this->fieldIdentifier',
                'string',
                $value->layoutIdentifier
            );
        }
    }


    /**
     * Returns information for FieldValue->$sortKey relevant to the field type.
     *
     * @param SPIValue|Value $value
     *
     * @return string
     */
    protected function getSortInfo(SPIValue $value): string
    {
        return mb_strtolower((string)$value);
    }

    /**
     * Converts an $hash to the Value defined by the field type.
     *
     * @param mixed $hash
     *
     * @return Value $value
     */
    public function fromHash($hash): Value
    {
        if ($hash === null) {
            return $this->getEmptyValue();
        }

        if (!is_string($hash)) {
            throw new DomainException('Unexpected hash type');
        }

        return new Value($hash);
    }

    /**
     * Converts a $Value to a hash.
     *
     * @param SPIValue|Value $value
     *
     * @return string|null
     */
    public function toHash(SPIValue $value): ?string
    {
        if (!$value instanceof Value) {
            throw new DomainException('Unexpected value type');
        }

        if ($this->isEmptyValue($value)) {
            return null;
        }

        return $value->layoutIdentifier;
    }
}
