<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Utils;

use DomainException;
use Ibexa\Contracts\Core\Repository\ContentTypeService;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use PHPUnit\Framework\ExpectationFailedException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Trait to help with functional tests of block builder page
 *
 * @property WebTestCase $webContext
 */
trait PageBuilderContextTrait
{
    private ContentTypeService $contentTypeService;
    private int $currentPageBuilderBlockIndex;

    /**
     * @BeforeScenario
     */
    public function setup(): void
    {
        $this->currentPageBuilderBlockIndex = 1;
    }

    /**
     * @param string $pageBuilderBlockType
     * @return string
     */
    private function registerAndGetPageBuilderBlockBaseSelector(string $pageBuilderBlockType): string
    {
        $selector = sprintf(".qa-item%d.%s", $this->currentPageBuilderBlockIndex++, $pageBuilderBlockType);

        try {
            $this->webContext::assertSelectorExists($selector);
        } catch (ExpectationFailedException $baseException) {
            try {
                $this->webContext::assertSelectorExists('.' . $pageBuilderBlockType);
                throw new ExpectationFailedException(
                    'Element seems to exists, but not in the right rank.'
                    . 'Take care that page builder block are order sentitive.'
                );
            } catch (ExpectationFailedException $e) {
                throw $baseException;
            }
        }

        return $selector;
    }

    /**
     * @param string $layoutLabel
     * @param string $contentTypeIdentifier
     * @return string
     */
    protected function getLayoutClass(string $layoutLabel, string $contentTypeIdentifier): string
    {
        try {
            $contentType = $this->contentTypeService->loadContentTypeByIdentifier($contentTypeIdentifier);
        } catch (NotFoundException $e) {
            throw new DomainException($e->getMessage(), 0, $e);
        }
        $layoutFieldDefinitionList = $contentType->getFieldDefinitionsOfType('isblocklayout');
        if ($layoutFieldDefinitionList->isEmpty()) {
            throw new DomainException(
                'Unable to find a layout field for content type : ' . $contentTypeIdentifier . '. '
                . 'Please create a field or fix the context file.'
            );
        }

        $layoutIdentifierByLabelList = [];
        foreach ($layoutFieldDefinitionList as $layoutFieldDefinition) {
            $options = $layoutFieldDefinition->fieldSettings['options'] ?? null;
            if (!is_array($options)) {
                throw new DomainException('Unexpected fieldSettings[\'options\'] type');
            }
            $layoutIdentifierByLabel = array_flip($options);
            $layoutIdentifierByLabelList[] = $layoutIdentifierByLabel;

            if (isset($layoutIdentifierByLabel[$layoutLabel])) {
                return (string)$layoutIdentifierByLabel[$layoutLabel];
            }
        }

        throw new DomainException(
            'Unknown layout label : "' . $layoutLabel . '". Existing label are : "'
            . implode('", "', array_keys(array_merge($layoutIdentifierByLabelList))) . '"'
        );
    }
}
