<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class InfostratesIbexaContentPageBuilderBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
    }
}
