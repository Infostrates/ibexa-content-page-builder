<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Actions;

use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\SortClause\ContentId;
use Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\CurrentUrlIsABlockContext;
use Infostrates\IbexaContentUtils\SearchServiceHelper;
use Infostrates\IbexaContentUtils\Traits\GetLocationFromView;
use Ibexa\Contracts\Core\Repository\ContentTypeService;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Ibexa\Core\MVC\Symfony\View\ContentView;
use LogicException;

class PageBuilderBlocks
{
    use GetLocationFromView;

    public const EMBED_CONTENT_LIST_PARAMETER_NAME = 'embed_content_list';

    private SearchServiceHelper $searchServiceHelper;
    private ContentTypeService $contentTypeService;
    private LocationService $locationService;
    private CurrentUrlIsABlockContext $currentUrlIsABlockContext;

    public function __construct(
        SearchServiceHelper $searchServiceHelper,
        ContentTypeService $contentTypeService,
        LocationService $locationService,
        CurrentUrlIsABlockContext $currentUrlIsABlockContext
    ) {
        $this->searchServiceHelper = $searchServiceHelper;
        $this->contentTypeService = $contentTypeService;
        $this->locationService = $locationService;
        $this->currentUrlIsABlockContext = $currentUrlIsABlockContext;
    }

    public function __invoke(ContentView $view): ContentView
    {
        $location = $this->getLocationFromView($view);

        $view->addParameters(
            [
                self::EMBED_CONTENT_LIST_PARAMETER_NAME => $this->getEmbedContentList($location),
            ]
        );

        return $view;
    }

    /**
     * @param Location|null $location
     * @return Content[]
     */
    protected function getEmbedContentList(?Location $location): array
    {
        if (!$location) {
            return [];
        }
        $contentList = $this->searchServiceHelper->loadDirectChildrenContentList(
            $location,
            $this->getEmbedContentTypeIdentifierList(),
            [],
            0,
            0,
            array_merge(
                $this->searchServiceHelper->getSortClausesFromLocation($location),
                [new ContentId()]
            )
        );

        if (
            $this->currentUrlIsABlockContext->hasContext()
            && $this->currentUrlIsABlockContext->getParentLocation()->id === $location->id
        ) {
            $this->insertPreviewContentInContentList(
                $contentList,
                $this->currentUrlIsABlockContext->getContent()
            );
        }
        return $contentList;
    }

    /**
     * @return string[]
     */
    private function getEmbedContentTypeIdentifierList(): array
    {
        try {
            $embedContentTypeGroup = $this->contentTypeService->loadContentTypeGroupByIdentifier('Page builder blocks');
        } catch (NotFoundException $e) {
            throw new LogicException('No Page builder blocks content type group');
        }
        $embedContentTypeList = $this->contentTypeService->loadContentTypes($embedContentTypeGroup);

        return array_map(
            static function (ContentType $contentType) {
                return $contentType->identifier;
            },
            (array)$embedContentTypeList
        );
    }

    /**
     * @param Content[] $contentList
     */
    protected function insertPreviewContentInContentList(array &$contentList, Content $previewBlockContent): void
    {
        $contentList = array_filter($contentList, static function (Content $content): bool {
            return $content->id !== ($previewBlockContent->id ?? null);
        });
        $contentList[] = $previewBlockContent;
        usort($contentList, function (Content $contentA, Content $contentB) {
            try {
                $locationA = $this->locationService->loadLocation((int)$contentA->contentInfo->mainLocationId);
                $priorityA = $locationA->priority;
            } catch (NotFoundException | UnauthorizedException $e) {
                $priorityA = 0;
            }

            try {
                $locationB = $this->locationService->loadLocation((int)$contentB->contentInfo->mainLocationId);
                $priorityB = $locationB->priority;
            } catch (NotFoundException | UnauthorizedException $e) {
                $priorityB = 0;
            }
            if ($priorityA === $priorityB) {
                return $contentA->id <=> $contentB->id;
            }

            return $priorityA <=> $priorityB;
        });
    }
}
