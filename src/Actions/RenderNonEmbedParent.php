<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Actions;

use DomainException;
use Exception;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Ibexa\Core\MVC\Symfony\View\ContentView;
use Infostrates\IbexaContentPageBuilder\Domains\PageBuilder\CurrentUrlIsABlockContext;
use Infostrates\IbexaContentUtils\Traits\GetLocationFromView;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RenderNonEmbedParent
{
    use GetLocationFromView;

    private HttpKernelInterface $httpKernel;
    private RequestStack $requestStack;
    private LocationService $locationService;
    private CurrentUrlIsABlockContext $currentUrlIsABlockContext;

    public function __construct(
        HttpKernelInterface $httpKernel,
        RequestStack $requestStack,
        LocationService $locationService,
        CurrentUrlIsABlockContext $currentUrlIsABlockContext
    ) {
        $this->httpKernel = $httpKernel;
        $this->requestStack = $requestStack;
        $this->locationService = $locationService;
        $this->currentUrlIsABlockContext = $currentUrlIsABlockContext;
    }

    /**
     * @param Request     $request
     * @param ContentView $view
     * @return Response
     * @throws Exception
     */
    public function __invoke(Request $request, ContentView $view): Response
    {
        $location = $this->getLocationFromView($view);
        if ($location === null) {
            $location = $this->findLocationFromRequest($request);

            if (!$location) {
                throw new RuntimeException('No location to get parent');
            }
        }

        $parentLocation = $this->locationService->loadLocation($location->parentLocationId);

        $this->currentUrlIsABlockContext->storeContext(
            $parentLocation,
            $location->getContent()
        );
        try {
            do {
                $location = $this->locationService->loadLocation($location->parentLocationId);
            } while ($this->isAPageBuilderBlockContentType($location->getContent()->getContentType()));
            $targetLocationId = $location->id;
        } catch (NotFoundException | UnauthorizedException $e) {
            throw new RuntimeException('Unable to get parent', $e->getCode(), $e);
        }

        return $this->forward(
            'ibexa_content:viewAction',
            [
                'locationId' => $targetLocationId,
                'viewType' => $view->getViewType(),
                'params' => [
                    'comeFromContentId' => $view->getContent()->id,
                ],
            ]
        );
    }

    /**
     * @param string               $controller
     * @param array<string, mixed> $path
     * @param array<string, mixed> $query
     * @return Response
     * @throws Exception
     */
    protected function forward(string $controller, array $path = [], array $query = []): Response
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request === null) {
            throw new RuntimeException('Unable to get request');
        }

        $path['_forwarded'] = $request->attributes;
        $path['_controller'] = $controller;
        $path['params']['siteaccess'] = $request->attributes->get('siteaccess');
        $subRequest = $request->duplicate($query, null, $path);

        return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }

    private function isAPageBuilderBlockContentType(ContentType $contentType): bool
    {
        foreach ($contentType->getContentTypeGroups() as $contentTypeGroup) {
            if ($contentTypeGroup->identifier === 'Page builder blocks') {
                return true;
            }
        }

        return false;
    }

    private function findLocationFromRequest(Request $request): ?Location
    {
        $location = $request->attributes->get('location');
        if ($location instanceof Location) {
            return $location;
        }

        return null;
    }
}
