<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentPageBuilder\Actions\PageBuilderBlock;

use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\SortClause\ContentId;
use Ibexa\Contracts\HttpCache\Handler\ContentTagInterface;
use FOS\HttpCacheBundle\Http\SymfonyResponseTagger;
use Infostrates\IbexaContentPageBuilder\Actions\PageBuilderBlocks;
use Infostrates\IbexaContentUtils\SearchServiceHelper;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Core\MVC\Symfony\View\ContentView;

class GenericParentPageBuilderBlock
{
    protected SearchServiceHelper $searchServiceHelper;
    protected LocationService $locationService;
    private SymfonyResponseTagger $responseTagger;

    public function __construct(
        SearchServiceHelper $searchServiceHelper,
        LocationService $locationService,
        SymfonyResponseTagger $responseTagger
    ) {
        $this->searchServiceHelper = $searchServiceHelper;
        $this->locationService = $locationService;
        $this->responseTagger = $responseTagger;
    }

    /**
     * @param ContentView $view
     * @return ContentView
     * @throws NotFoundException
     * @throws UnauthorizedException
     */
    public function __invoke(ContentView $view): ContentView
    {
        $location = $this->locationService->loadLocation((int)$view->getContent()->contentInfo->mainLocationId);

        $view->addParameters([
            PageBuilderBlocks::EMBED_CONTENT_LIST_PARAMETER_NAME => $this->getEmbedContentList($location),
        ]);

        return $view;
    }


    /**
     * @param Location|null $location
     * @return Content[]
     */
    protected function getEmbedContentList(?Location $location): array
    {
        if (!$location) {
            return [];
        }

        $this->responseTagger->addTags([ContentTagInterface::LOCATION_PREFIX . $location->id]);

        return $this->searchServiceHelper->loadDirectChildrenContentList(
            $location,
            $this->getEmbedContentContentTypeIdentifierList($location),
            [],
            0,
            0,
            array_merge(
                $this->searchServiceHelper->getSortClausesFromLocation($location),
                [new ContentId()]
            )
        );
    }

    /**
     * @param Location $location
     * @return string[]
     */
    protected function getEmbedContentContentTypeIdentifierList(Location $location): array
    {
        return [$location->getContent()->getContentType()->identifier . '_item'];
    }
}
