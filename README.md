Infostrates Ibexa Content Page builder
======================================

A bundle to help make Page Builder pages "à la" Infostrates.
It provides :

- Simple ```page_builder_blocks()``` twig function to display the page builder under the current location.
- In Ibexa Admin UI, previewing while modifying a bloc builder will preview the closest parent page to show the block in
  its context.
- A simple "block with children" controller
- A 'isblocklayout' field type which is similar to ezselection, but with custom identifier in key, and a default value.
- A non-mandatory BDD workflow described below

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer config repositories.infostrates_ibexa_content_page_builder vcs https://bitbucket.org/Infostrates/ibexa-content-page-builder.git
$ composer require infostrates/ibexa-content-page-builder
```

### Step 2: Only for applications that don't use Symfony Flex, enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Infostrates\IbexaContentPageBuilder\InfostratesIbexaContentPageBuilderBundle::class => ['all' => true],
];
```

### Step 3: Run tanoconsulting/ezmigrationbundle2 migration

Check that ```src/MigrationsDefinitions/20220322102800_pageBuilderGroup.yml``` has been executed

### Step 4: Copy the config file

Copy src/Resources/config_model/ezplatform_pagebuilder.yaml in your config/packages folder.
Note that this step could be replaced by a flex recipe ;)

### Step 5: Start creating your own migration to create blocks

All block content type should be put in the content type group "Page builder blocks".
I advise you to create all the content types and example contents in the same migration (with changes tracked to
rebuild,
like https://bitbucket.org/Infostrates/00-dave-boilerplate-ibexa-dxp-3.3/src/main/README.md#markdown-header-utilisation-avancee-changeable-migrations)

Here's a full example on a real
project : https://bitbucket.org/Infostrates/ly-01-vacances-bleues-refonte-site/src/develop/migrations/20220322144900_pageBuilder.yml

Note that :

- Fake content creation is conditioned by the "migrate_fake_data" parameter => help to get a clean installation for
  prod, or useful example for dev envs
- The "Page builder" content type and content where created in a previous migration (base structure, that helps to get
  the main layout fully ready before all the features are defined)
- Follows all the content types definitions, all in the "Page builder blocks" content type group
- Finally, all the fake content are in a loop to have a single condition on "migrate_fake_data".

## Steps to create your own block

For each block :

- Write the functional test in your page builder's feature file, and complete the appropriate context. See below for
  examples.
- Write your block content type and fake content migration definition. Your new content type MUST be in the
  content_type_group "Page builder blocks", and its identifier SHOULD start with "pbb_".
- Create your template in your templates/themes/standard/page-builder-blocks folder. Make it extends '
  @ezdesign/page-builder-blocks/layout.html.twig' to get the basic layout which will help for functionnal tests.
- Create the controller if needed. You might want to use
  Infostrates\IbexaContentPageBuilder\Actions\PageBuilderBlock\GenericParentPageBuilderBlock for basic children support
  with "<parent identifier>_item" as identifier, or extend it to customize children content type(s). It will help with
  the preview feature.
- Add your rule in config/packages/ezplatform_pagebuilder.yaml under the pageBuilderBlock node.

### Create your functional tests (Behat)

There's a utility trait (Infostrates\IbexaContentPageBuilder\Utils\PageBuilderContextTrait) that can be used to ease the
making of page builder tests.

Example :

Feature file : https://bitbucket.org/Infostrates/ly-01-vacances-bleues-refonte-site/src/develop/features/pages/19.page_builder.feature

Context, using the utility trait : https://bitbucket.org/Infostrates/ly-01-vacances-bleues-refonte-site/src/develop/tests/Behat/Modules/PageBuilderContext.php

## Contributing to this bundle

No dev stack for now, you'll have to install php on your workspace. CI tests can be run locally with :

```
make test
```

Versions are made simply by tagging the desired commit (don't forget to push the tags !).
